import { CallPrinter } from '../contracts/call-printer-contract'
import { TemplateParser } from '../provider/template-parser/template-parser'

import { exec } from 'child_process'

class CallPrinterCupsService implements CallPrinter {
  constructor (private readonly templateParser: TemplateParser) {}

  print (template: string, values: CallPrinter.Value): void {
    const templateParsed = this.templateParser.parse(template, values)

    exec(`echo "${templateParsed}"| lpr`, (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`)
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`)
      }
      console.log(`stdout: ${stdout}`)
    })
  }
}

export { CallPrinterCupsService }
