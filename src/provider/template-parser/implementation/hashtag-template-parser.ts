import { TemplateParser } from '../template-parser'

class HashTagTemplateParser implements TemplateParser {
  parse (template: string, values: TemplateParser.Value): string {
    let templateParsed = template
    const keys = Object.keys(values)

    for (const key of keys) {
      templateParsed = templateParsed.split('#' + key + '#').join(values[key])
    }

    return templateParsed
  }
}

export { HashTagTemplateParser }
