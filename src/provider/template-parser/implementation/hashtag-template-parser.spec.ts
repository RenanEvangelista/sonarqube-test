import { HashTagTemplateParser } from './hashtag-template-parser'

const makeSut = (): HashTagTemplateParser => {
  return new HashTagTemplateParser()
}

describe('HashTag Template Parser', () => {
  test('Should be call parser with correct values', () => {
    const sut = makeSut()
    const parseSpy = jest.spyOn(sut, 'parse')

    sut.parse('any template', { any_key: 'any_value' })
    expect(parseSpy).toHaveBeenCalledWith('any template', { any_key: 'any_value' })
  })

  test('Should be able to parser', () => {
    const sut = makeSut()

    const template = sut.parse('welcome #user# user', { user: 'renan' })
    expect(template).toBe('welcome renan user')
  })
})
