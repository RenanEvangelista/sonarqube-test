import { Router, Request, Response } from 'express'
import { makeCallPrinterService } from '../factories/make-call-printer-service'

export default (router: Router): void => {
  router.post('/print', (req: Request, res: Response) => {
    const { template, values } = req.body

    const callPrinterService = makeCallPrinterService()
    callPrinterService.print(template, values)
    return res.send('Called')
  })
}
